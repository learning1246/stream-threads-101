package com.practice.practice.dto;

import java.util.List;

public class Person {
    long personId;
    String name;
    int age;
    double salary;
    String role;
    List<String> phoneNumbers;
    List<String> hobbies;

    public Person(long personId, String name, int age, double salary, String role) {
        this.personId = personId;
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.role = role;
    }

    public Person(long personId, String name, int age, double salary, String role, List<String> phoneNumbers) {
        this.personId = personId;
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.role = role;
        this.phoneNumbers = phoneNumbers;

    }

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Person [age=" + age + ", name=" + name + ", personId=" + personId + ", role=" + role + ", salary="
                + salary + "]";
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

}
