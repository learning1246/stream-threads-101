package com.practice.practice.data;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.practice.practice.dto.Employee;
import com.practice.practice.dto.Person;

public class MockData {

    public static List<Person> getPersons() {

        Person[] p1 = {
                new Person(1, "Tom", 40, 200.00, "Security", Arrays.asList("993", "893")),
                new Person(2, "Jerry", 21, 1200.00, "Security", Arrays.asList("112", "231")),
                new Person(3, "Dwigthd", 26, 1000.00, "Sales", Arrays.asList("231", "245")),
                new Person(4, "Jim", 25, 2200.00, "Sales", Arrays.asList("331", "794")),
                new Person(5, "Pam", 25, 2200.00, "Sales", Arrays.asList("593", "594")),
                new Person(6, "Micheal", 40, 2000.00, "Manager", Arrays.asList("893", "231")),
                new Person(7, "Kevin", 45, 1000.00, "Accounting", Arrays.asList("893", "794")),
                new Person(8, "Kelly", 23, 500.00, "Accounting", Arrays.asList("893", "894"))

        };
        List<Person> personList = Arrays.asList(p1);
        return personList;
    }

    public static void delay(int delay) {
        try {
            TimeUnit.SECONDS.sleep(delay);
        } catch (InterruptedException e) {

        }
    }

    public static List<Employee> fetchEmployees() {
        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper
                    .readValue(new File("src/main/resources/employees.json"), new TypeReference<List<Employee>>() {
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
