package com.practice.practice.oct.a20221023;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class ThreadLocalDemo {
    public static void main(String[] args) {

        new Thread(new TL("DJ", new TLObject())).start();
        new Thread(new TL("Rdivi", new TLObject())).start();

    }
}

class UserDataHolder {
    public static ThreadLocal<String> userData = new ThreadLocal<>();
}

class TLObject {

    public void display() {
        System.out.println("Display is called by " + UserDataHolder.userData.get());
        new TL2Object().display();
    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class TL implements Runnable {

    String name;
    TLObject tlObject;

    @Override
    public void run() {
        UserDataHolder.userData.set(name);
        tlObject.display();
    }

}

class TL2Object {

    public void display() {
        System.out.println("TL2Object Display is called by " + UserDataHolder.userData.get());
    }

}