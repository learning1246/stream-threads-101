package com.practice.practice.oct.a20221005;

public class SortingDemo {

    public static void main(String[] args) {

        int input[] = { 3, 2, 4, 1, 8, 10 };

        // int[] result = selectionSort(input);
        // int[] result = quickSort(input, 0, input.length - 1);
        // for (int i : result) {
        //     System.out.println(i);
        // }
        mergeSort(input, 0, input.length - 1);
        for (int i : input) {
            System.out.println(i);
        }

    }

    public static int[] bubbleSort(int input[]) {
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input.length - 1 - i; j++) {
                if (input[j] > input[j + 1]) {
                    swap(input, j + 1, j);
                }
            }
        }
        return input;
    }

    public static int[] insertionSort(int input[]) {
        for (int i = 1; i < input.length; i++) {
            int temp = input[i];
            int j = i - 1;
            while (j >= 0 && input[j] > temp) {
                input[j + 1] = input[j];
                j--;
            }
            input[j + 1] = temp;
        }
        return input;
    }

    public static int[] selectionSort(int input[]) {
        for (int i = 0; i < input.length; i++) {
            int min = i;
            for (int j = i + 1; j < input.length; j++) {
                if (input[j] < input[min]) {
                    min = j;
                }
            }
            if (min != i) {
                swap(input, i, min);
            }
        }
        return input;
    }

    public static int[] quickSort(int input[], int l, int h) {
        if (l < h) {
            int pivot = partition(input, l, h);
            partition(input, l, pivot - 1);
            partition(input, pivot - 1, h);
        }
        return input;
    }

    public static int partition(int[] input, int l, int h) {
        int pivot = input[l];
        int i = l;
        int j = h;
        while (i < j) {
            while (input[i] <= pivot) {
                i++;
            }
            while (input[j] > pivot) {
                j--;
            }
            if (i < j) {
                swap(input, i, j);
            }
        }
        swap(input, l, j);
        return j;
    }

    public static void mergeSort(int input[], int l, int h) {
        if (l < h) {
            int mid = (l + h) / 2;
            mergeSort(input, l, mid);
            mergeSort(input, mid + 1, h);
            merge(input, l, mid, h);
        }
    }

    private static void merge(int[] input, int l, int mid, int h) {
        int b[] = new int[input.length];
        int i = l;
        int j = mid + 1;
        int k = l;
        while (i <= mid && j <= h) {
            if (input[i] < input[j]) {
                b[k] = input[i];
                i++;
            } else {
                b[k] = input[j];
                j++;
            }
            k++;
        }
        if (i > mid) {
            while (j <= h) {
                b[k] = input[j];
                j++;
                k++;
            }
        } else {
            while (i <= mid) {
                b[k] = input[i];
                i++;
                k++;
            }
        }
        for (int x = l; x <= h; x++) {
            input[x] = b[x];
        }
    }

    public static void swap(int[] input, int i, int j) {
        int temp = input[i];
        input[i] = input[j];
        input[j] = temp;
    }
}
