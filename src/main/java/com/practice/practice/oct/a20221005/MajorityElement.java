package com.practice.practice.oct.a20221005;


public class MajorityElement {

    public static void main(String[] args) {
        MajorityElement m = new MajorityElement();
        m.majorityElement(new int[]{1,1,0});
    }

    // DO NOT MODIFY THE ARGUMENTS WITH "final" PREFIX. IT IS READ ONLY
    public int majorityElement(final int[] A) {
        int current = 0;
        int count = 1;
        for(int i=0;i<A.length;i++){
            if(A[current]==A[i]){
                current=i;
                count++;
            }else{
                count--;
            }
            if(count<=0){
                current=i;
                count=1;
            }
        }
        if(count>(A.length/2)){
            return A[current];
        }
        
        return 0;
    }
}
