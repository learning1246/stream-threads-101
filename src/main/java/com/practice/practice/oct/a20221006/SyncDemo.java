package com.practice.practice.oct.a20221006;

import java.util.Arrays;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public class SyncDemo {

    public static void main(String[] args) throws InterruptedException {

       

        Integer[] arr = new Integer[] { 10, 3, 10, 2, 20 };
        System.out.println(
                Arrays.asList(arr).stream().mapToInt(n -> n * n).filter(n -> n >= 10).average().getAsDouble());
        ;

    }

}

class Car<T> {
    void set(T t) {

    }
}

class A {
    public void print() {
        System.out.println("Printing A");
    }
}

class B extends A {
    public void print() {
        System.out.println("Printing B");
    }
}

class C extends B {
    public void print() {
        super.print();
        System.out.println("Printing c");
    }
}
