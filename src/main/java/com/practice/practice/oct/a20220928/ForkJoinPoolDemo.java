package com.practice.practice.oct.a20220928;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class ForkJoinPoolDemo {

    public static void main(String[] args) {

        ForkJoinPool pool = new ForkJoinPool();
        Integer invoke = pool.invoke(new Fibo(6));
        System.out.println(invoke);

    }

}

class Fibo extends RecursiveTask<Integer> {

    int n;

    Fibo(int n) {
        this.n = n;
    }

    @Override
    protected Integer compute() {
        if (n <= 1) {
            return n;
        }
        Fibo f1 = new Fibo(n - 1);
        f1.fork();
        Fibo f2 = new Fibo(n - 2);
        f2.fork();
        return f1.join() + f2.join();
    }

}