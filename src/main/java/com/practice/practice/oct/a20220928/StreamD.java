package com.practice.practice.oct.a20220928;

import java.util.stream.Collectors;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;

public class StreamD {
    public static void main(String[] args) {

        Object count = MockData.fetchEmployees().stream()
                .collect(Collectors.groupingBy(Employee::getGender, Collectors.counting()));
        System.out.println(count);
    }
}
