package com.practice.practice.oct.a20220928;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;

public class SupplyAsync {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> MockData.fetchEmployees())
                .thenApply((emps) -> {
                    System.out.println("Get All EMployees Thread name: " + Thread.currentThread().getName());
                    return emps.stream().filter(e -> e.getNewJoiner().equalsIgnoreCase("true"))
                            .collect(Collectors.toList());
                }).thenApply((emps) -> {
                    System.out.println("Get New employee Thread name: " + Thread.currentThread().getName());
                    return emps.stream().filter(e -> e.getLearningPending().equalsIgnoreCase("true"))
                            .collect(Collectors.toList());
                }).thenApply((emps) -> {
                    System.out.println("Get training pending empliyee Thread name: " + Thread.currentThread().getName());
                    return emps.stream().map(Employee::getEmail);
                }).thenAccept((emailIds) -> {
                    System.out.println("Send email Id Thread name: " + Thread.currentThread().getName());
                    emailIds.forEach(email -> {
                        System.out.println("Sending email to email id: " + email);
                    });
                });
        future.get();
    }
}
