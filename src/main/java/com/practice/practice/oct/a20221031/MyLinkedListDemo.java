package com.practice.practice.oct.a20221031;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;



public class MyLinkedListDemo {
    public static void main(String[] args) {
        MyLinkedList obj = new MyLinkedList();

        obj.addAtHead(4);
        System.out.print("After add to head: ");
        obj.display();
        obj.addAtHead(1);
        System.out.print("After add to head: ");
        obj.display();
        obj.addAtHead(5);
        System.out.print("After add to head: ");
        obj.display();
        obj.deleteAtIndex(3);
        System.out.print("After delete from index: ");
        obj.display();
        obj.addAtHead(7);
        System.out.print("After add to head: ");
        obj.display();
        System.out.println(obj.get(3));
        System.out.println(obj.get(3));
        System.out.println(obj.get(3));
        obj.addAtHead(1);
        System.out.print("After add to head: ");
        obj.display();
        obj.deleteAtIndex(4);    
        System.out.print("After delete from index: ");
        obj.display();    
    }
}

class MyLinkedList {

    int data;
    MyLinkedList next;
    MyLinkedList head;

    public String toString(){
        return this.data +"";
    }

    public MyLinkedList() {
        next=null;
        head=null;
    }

    public MyLinkedList(int data) {
        this.data = data;
    }

    public int get(int index) {
        if (index == 0) {
            return head.data;
        }
        MyLinkedList current = head;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }
        if (current != null) {
            return current.data;
        }
        return -1;
    }

    public void addAtHead(int val) {
        MyLinkedList nodeToAdd = new MyLinkedList(val);
        if (head != null) {
            nodeToAdd.next = head;
        }
        head = nodeToAdd;
    }

    public void addAtTail(int val) {
        MyLinkedList nodeToAdd = new MyLinkedList(val);
        if (head == null) {
            head = nodeToAdd;
            return;
        }
        MyLinkedList current = head;
        while (current.next != null) {
            current = current.next;
        }
        current.next = nodeToAdd;
    }

    public void addAtIndex(int index, int val) {
        MyLinkedList toAdd = new MyLinkedList(val);
        if (index == 0) {
            toAdd.next = head;
            head = toAdd;
            return;
        }
        MyLinkedList prev = head;
        for (int i = 0; i < index - 1; i++) {
            if (prev.next == null) {
                break;
            }
            prev = prev.next;
        }
        toAdd.next = prev.next;
        prev.next = toAdd;
    }

    public void display() {
        if (head == null) {
            System.out.println("Linked list is empty");
            return;
        }
        MyLinkedList currentNode = head;
        while (currentNode != null) {
            System.out.print(currentNode.data + " ");
            currentNode = currentNode.next;
        }
        System.out.println();
    }

    public void deleteAtIndex(int index) {
        if (index == 0) {
            head = head.next;
            return;
        }
        MyLinkedList currentNode = head;
        MyLinkedList prev = head;

        for (int i = 0; i < index; i++) {
            prev = currentNode;
            currentNode = currentNode.next;
        }
        if(currentNode!=null){
        prev.next = currentNode.next;
        }
    }

    public String tournamentWinner(
      ArrayList<ArrayList<String>> competitions, ArrayList<Integer> results) {
    // Write your code here.
    Map<String,Integer> map = new HashMap<>();
    for(int i=0;i<results.size();i++){
      int result = results.get(i);
      if(result == 1){
        String team = competitions.get(i).get(0);
        int count = 0;
        if(map.containsKey(team)){
          count=map.get(team);
        }
        map.put(team,count);
      }
    }

    if(map.size()==1){
        return map.keySet().stream().findFirst().get();
      }

    String winner = map.entrySet().stream().sorted(Comparator.comparing(Entry::getValue,Comparator.reverseOrder())).limit(1).findFirst().get().getKey();
    return winner;
  }
}
