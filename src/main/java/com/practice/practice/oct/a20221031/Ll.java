package com.practice.practice.oct.a20221031;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class Ll {

    public static void main(String[] args) {
        DLinkedList d = new DLinkedList();
        d.add(10);
        d.add(50);
        d.add(5);
        d.add(40, 2);
        d.display();
        System.out.println("\nAfter delete");
        d.delete(1);
        d.display();
    }

}

@Data
@NoArgsConstructor
class DLinkedList {
    int data;
    DLinkedList next;
    DLinkedList head = null;

    public DLinkedList(int data) {
        this.data = data;
        this.next = null;
    }

    public void add(int data) {
        if (head == null) {
            head = new DLinkedList(data);
            return;
        }
        DLinkedList currentNode = head;
        while (currentNode.next != null) {
            currentNode = currentNode.next;
        }
        DLinkedList dLinkedList = new DLinkedList(data);
        currentNode.next = dLinkedList;
    }

    public void add(int data, int location) {
        DLinkedList toAdd = new DLinkedList(data);
        if (location == 0) {
            toAdd.next = head;
            head = toAdd;
            return;
        }
        DLinkedList prev = head;
        for (int i = 0; i < location - 1; i++) {
            if (prev.next == null) {
                break;
            }
            prev = prev.next;
        }
        toAdd.next = prev.next;
        prev.next = toAdd;
    }

    public void delete(int location) {
        if (location == 0) {
            head = head.next;
            return;
        }
        DLinkedList currentNode = head;
        DLinkedList prev = head;

        for (int i = 0; i < location; i++) {
            if (currentNode.next == null) {
                break;
            }
            prev = currentNode;
            currentNode = currentNode.next;
        }
        prev.next = currentNode.next;
    }

    public void display() {
        if (head == null) {
            System.out.println("Linked list is empty");
            return;
        }
        DLinkedList currentNode = head;
        while (currentNode != null) {
            System.out.print(currentNode.data + " ");
            currentNode = currentNode.next;
        }
    }

}