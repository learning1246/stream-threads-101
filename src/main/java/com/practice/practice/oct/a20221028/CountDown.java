package com.practice.practice.oct.a20221028;

import java.util.Comparator;
import java.util.OptionalDouble;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Stream;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class CountDown {

    public static void main(String[] args) throws InterruptedException {

        Integer average = MockData.fetchEmployees().stream().map(Employee::getRating).reduce(Integer.MAX_VALUE,Math::min);
        System.out.println(average);
        // CountDownLatch cl = new CountDownLatch(3);
        // Stream.of(
        //         new CDL(cl, "Dips", 2),
        //         new CDL(cl, "DJ", 10),
        //         new CDL(cl, "Ridhvi", 5))
        //         .sorted(Comparator.comparing(CDL::getDelay).reversed())
        //         .forEach(t -> {
        //             System.out.println(t.getDelay());
        //             new Thread(t).start();
        //         });

        // System.out.println("Main thread waiting : " + cl.getCount());
        // cl.await();
        // System.out.println("Main thread can continue");
    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class CDL implements Runnable {

    private CountDownLatch cl;
    private String name;
    private int delay;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        System.out.println(name + " is executing");
        MockData.delay(delay);
        System.out.println(name + " counting down");
        cl.countDown();
    }

}