package com.practice.practice.oct.a20221028;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class MajorityElement {

    public static void main(String[] args) {
        int[] arr = { 3,2,3 };
        // Comparator<Entry<int[], Long>> comparing =
        // Comparator.comparing(Entry::getValue);

        // Comparator<Entry<Integer, Long>> comparing2 =
        // Comparator.comparing(Entry::getValue);
        // Optional<Entry<Integer, Long>> entrySet = Stream.of(5, 4, 5, 5, 1)
        // .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
        // .entrySet().stream().filter(p-> p.getValue() > (arr.length)/2).findAny();
        // if (entrySet.isPresent()) {
        // System.out.println("Major:" + entrySet.get().getKey());
        // } else {
        // System.out.println("Major: " + -1);
        // }

        Map<Integer,Integer> map =new HashMap<>();
        for(int i:arr){
            if(map.containsKey(i)){
                map.put(i,map.get(i)+1);
            }else{
                map.put(i, 1);
            }
        }

        List<Integer> collect = map.entrySet().stream().filter(e-> e.getValue()> (arr.length)/3).map(Entry::getKey).collect(Collectors.toList());
        System.out.println(collect);
    }
}
