package com.practice.practice.oct.a20221028;

import org.springframework.util.SystemPropertyUtils;

public class kaden {

    public static void main(String[] args) {
        int[] input = {-2, -3, 4, -1, -2, 1, 5, -3};

        int maxSoFar = Integer.MIN_VALUE;
        int maxSum = 0;
        for (int i = 0; i < input.length; i++) {
            maxSum+=input[i];
            maxSoFar = Math.max(maxSum, maxSoFar);
            if(maxSum<0){
                maxSum=0;
            }
        }

        System.out.println(maxSoFar);

    }

}
