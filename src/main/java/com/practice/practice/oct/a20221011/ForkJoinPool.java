package com.practice.practice.oct.a20221011;

import java.util.stream.Collectors;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;

public class ForkJoinPool {

    public static void main(String[] args) {
        Object collect = MockData.fetchEmployees().stream().filter(e -> e.getFirstName().startsWith("A"))
                .skip(30)
                .peek(e -> System.out.println(e.getEmployeeId()))
                .collect(Collectors.groupingBy(Employee::getNewJoiner, Collectors.counting()));
        System.out.println(collect);
    }

}