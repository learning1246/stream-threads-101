package com.practice.practice.oct.a20221010;

import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class ThreadDemo {

    public static void main(String[] args) {

        RequestThread t[] = {

                new RequestThread("233"),
                new RequestThread("123"),
                new RequestThread("423")
        };

        Stream.of(

                new RequestThread("233"),
                new RequestThread("123"),
                new RequestThread("423")).forEach(ts -> {
                    new Thread(ts).start();
                });

    }

}

@Data
@AllArgsConstructor
@NoArgsConstructor
class RequestThread implements Runnable {

    String itemId;

    @Override
    public void run() {
        new Controller().doTransaction(itemId);
    }

}

class Context {
    public static ThreadLocal<String> userData = new ThreadLocal<String>() {
        @Override
        protected String initialValue() {
            return "DJ";
        }
    };
}

class Controller {

    public void doTransaction(String itemId) {
        Context.userData.set(itemId);
        System.out.println("Reservring quantity by " + Thread.currentThread().getName());
        new Service().payUp(itemId);
    }
}

class Service {

    public void payUp(String itemId) {
        String string = Context.userData.get();
        System.out.println(
                "Payment for  quantity by " + Thread.currentThread().getName() + "    Item Id from tl: " + string);
        System.out.println(
                "Payment for  quantity by " + Thread.currentThread().getName() + "    Item Id from method: " + itemId);
        Context.userData.remove();
        System.out.println(
                "tem Id from method: " + Context.userData.get());
    }

}
