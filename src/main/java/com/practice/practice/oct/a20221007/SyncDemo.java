package com.practice.practice.oct.a20221007;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;

public class SyncDemo {

    public static void main(String[] args) {
        // Car c = new Car();
        // Stream.of(new Ridh(c, "Ridh"), new Ridh(c, "Dj"), new Ridh(c,
        // "Vinay")).forEach(x -> {
        // new Thread(x).start();

        // });
        // ;
        long start = System.currentTimeMillis();
        MockData.fetchEmployees().stream().forEach(System.out::print);
        long total = System.currentTimeMillis() - start;
        System.out.println("tota time: " + (total) );
    }

}

class Ridh implements Runnable {

    Car car;
    String name;

    public Ridh(Car car, String name) {
        this.car = car;
        this.name = name;
    }

    @Override
    public void run() {
        car.drive(name);
    }
}

class Car {

    Semaphore lock = new Semaphore(3);

    public void drive(String name) {
        System.out.println(name + " is now waiting for ghat section");

        try {
            lock.acquire();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        MockData.delay(1);
        System.out.println(name + " started climbing ghat");
        MockData.delay(3);
        System.out.println(name + " descended ghat");
        MockData.delay(1);

        System.out.println(name + " crossed bridge \n");
        MockData.delay(1);
        drift(name);

    }

    public void drift(String name) {
        MockData.delay(1);
        System.out.println(name + " now drifting \n");
        MockData.delay(1);
        lock.release();
    }
}