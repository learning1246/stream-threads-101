package com.practice.practice.oct.a20221017;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.practice.practice.data.MockData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class ES {

    public static void main(String[] args) {

        CountDownLatch cl = new CountDownLatch(1);

        EST arr[] = {
                new EST("Jim", cl),
                new EST("Pam", cl),
                new EST("Jerry", cl),
                new EST("Mike", cl),
                new EST("Dwight", cl),
        };

        ExecutorService es = Executors.newFixedThreadPool(5);
        List<Future> collect = Arrays.asList(arr).stream().map(e -> es.submit(e)).collect(Collectors.toList());
        collect.forEach( x-> {
            try {
                System.out.println(x.get());
            } catch (InterruptedException | ExecutionException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        });
    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class EST implements Callable {

    String name;
    CountDownLatch cl;

    @Override
    public Object call() {
        // TODO Auto-generated method stub
        MockData.delay(5);

        System.out.println(name + " Started and finished");
        return name + " Started and finished";
    }

}
