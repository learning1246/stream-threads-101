package com.practice.practice.oct.a20221004;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;

public class StreamDemo {
    public static void main(String[] args) {

        AtomicInteger ai = new AtomicInteger();

        int collect = MockData.fetchEmployees().stream()
                .filter(e -> e.getNewJoiner().equalsIgnoreCase("false"))
                .limit(3)
                .map(Employee::getSalary)
                .reduce(0,(a,b) -> {ai.incrementAndGet();return a+b;});

        System.out.println(collect/ai.get());
    }

}
