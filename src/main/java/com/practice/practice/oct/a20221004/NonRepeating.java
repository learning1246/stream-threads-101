package com.practice.practice.oct.a20221004;

public class NonRepeating {

    public static void main(String[] args) {
        NonRepeating r = new NonRepeating();
        System.out.println(r.solve(18));
    }

    public int solve(int A) {
        String input = Integer.toBinaryString(A);
        System.out.println(input);
        
        return input.length() - input.lastIndexOf('1')-1;
    }

}
