package com.practice.practice.oct.a20221030;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;

public class CompletableFutur {

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        // CompletableFuture<Void> thenAccept = CompletableFuture.supplyAsync(() -> {
        // return MockData.fetchEmployees();
        // }).thenApply(l -> {
        // return l.stream().filter(e ->
        // e.getNewJoiner().equalsIgnoreCase("true")).collect(Collectors.toList());
        // }).thenApply(newJoiners -> {
        // return
        // newJoiners.stream().map(Employee::getEmail).collect(Collectors.toList());
        // }).thenAccept(emailList -> {
        // emailList.stream().forEach(System.out::println);
        // });

        // thenAccept.get();

        CompletableFuture north = CompletableFuture.supplyAsync(() -> {
            MockData.delay(10);
            return 200;
        });

        CompletableFuture east = CompletableFuture.supplyAsync(() -> {
            MockData.delay(12);
            return 1200;
        });

        CompletableFuture south = CompletableFuture.supplyAsync(() -> {
            MockData.delay(15);
            return 5000;
        });

        CompletableFuture west = CompletableFuture.supplyAsync(() -> {
            MockData.delay(5);
            return 500;
        });

        System.out
                .println(Stream.of(east, west, north, south).map(CompletableFuture::join)
                        .reduce(0, (a,b)-> Integer.parseInt(a.toString()) + Integer.parseInt(b.toString())));

    }

}
