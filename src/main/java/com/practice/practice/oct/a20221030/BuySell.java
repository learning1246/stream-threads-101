package com.practice.practice.oct.a20221030;

import java.lang.reflect.Array;

public class BuySell {

    public static void main(String[] args) {
        System.out.println(maxProfit(new int[] { 3, 3, 5, 0, 0, 3, 1, 4 }));
    }

    public static int maxProfit(int[] prices) {
        int profit = 0;
        int count = 2;
        int lastProfit = 0;
        for (int i = 1; i < prices.length; i++) {
            int currentProfit = prices[i] - prices[i - 1];
            if (currentProfit > 0) {
                if (count > 0) {
                    if (currentProfit > lastProfit) {
                        profit -= lastProfit;
                        profit += currentProfit;
                        lastProfit = currentProfit;
                    } else {
                        profit += currentProfit;
                        lastProfit = currentProfit;
                        count--;
                    }
                } else {
                    if (currentProfit > lastProfit) {
                        profit -= lastProfit;
                        profit += currentProfit;
                        lastProfit = currentProfit;
                    }
                }
            }
        }
        return profit;
    }
}