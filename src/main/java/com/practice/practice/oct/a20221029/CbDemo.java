package com.practice.practice.oct.a20221029;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.stream.Stream;

import com.practice.practice.data.MockData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class CbDemo {

    public static void main(String[] args) {

        CyclicBarrier cb = new CyclicBarrier(3);
        Stream.of(
                new CDL(cb, "Dips", 2),
                new CDL(cb, "DJ", 10),
                new CDL(cb, "Ridhvi", 5)).forEach(t -> new Thread(t).start());
        System.out.println("Main thread droped");
    }
    
}
@Data
@NoArgsConstructor
@AllArgsConstructor
class CDL implements Runnable {

    private CyclicBarrier cb;
    private String name;
    private int delay;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        System.out.println(name + " is executing");
        MockData.delay(delay);
        System.out.println(name + " now waiting down");
        try {
            cb.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(name + " now started running down");
    }

}