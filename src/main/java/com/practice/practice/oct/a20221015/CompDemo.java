package com.practice.practice.oct.a20221015;

import java.util.concurrent.ExecutionException;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Person;

public class CompDemo {

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        Double reduce = MockData.getPersons()
        .stream()
        .map(Person::getSalary)
        .reduce(Double.MAX_VALUE, Math::min);
        System.out.println(reduce);
    }

}
