package com.practice.practice.oct.a20221022;

import java.util.stream.Collectors;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;

public class StreamDemo {

    public static void main(String[] args) {

        Object collect = MockData.fetchEmployees()
                .stream()
                .filter(x -> x.getRating() == 5)
                .peek(x -> System.out.println(x.getEmail()))
                .collect(Collectors.toMap(Employee::getEmployeeId,Employee::getLastName));

        System.out.println(collect);

    }

}
