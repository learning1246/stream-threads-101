package com.practice.practice.oct.a20221013;

import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Person;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class GoaTrip {
    public static void main(String[] args) {

        // CyclicBarrier cb = new CyclicBarrier(4);
        // Stream.of(
        //         new Amigo("Dj", cb, 4),
        //         new Amigo("Dwight", cb, 6),
        //         new Amigo("Pam", cb, 1),
        //         new Amigo("Jim", cb, 10)).forEach(a -> new Thread(a).start());'
        

        Object collect = MockData.getPersons().stream().map(Person::getPhoneNumbers).flatMap(List::stream).collect(Collectors.toList());
        System.out.println(collect);
    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Amigo implements Runnable {

    String name;
    CyclicBarrier cb;
    int waitTime;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        MockData.delay(waitTime);
        System.out.println(name + " has arrived to thane station, waiting for others");
        try {
            cb.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(name + " boarded the train");
    }

}