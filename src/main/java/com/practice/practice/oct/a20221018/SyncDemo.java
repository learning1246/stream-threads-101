package com.practice.practice.oct.a20221018;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Stream;

import com.practice.practice.data.MockData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class SyncDemo {

    public static void main(String[] args) {
        // Object collect = MockData.getPersons()
        // .stream()
        // .collect(Collectors.summingDouble(Person::getSalary));
        // System.out.println(collect);

        DatabaseConnectionPool dl = new DatabaseConnectionPool();

        DatabaseConnectionPool dl2 = new DatabaseConnectionPool();

        Stream.of(new DataWriter("Ridhvi", dl), new DataWriter("Jay", dl), new DataWriter("RJam", dl))
                .forEach(x -> new Thread(x).start());
    }
}

class DatabaseConnectionPool {

    public synchronized void getConnection(String name) {
        System.out.println(name + ": now got connection");
    }

    Semaphore lock = new Semaphore(2);

    public void writeData(String name) {

        if (lock.tryAcquire()) {
            System.out.println(name + ": now got connection");
            MockData.delay(2);
            System.out.println(name + ": is now writing data");
            MockData.delay(2);
            lock.release();
        } else {
            System.out.println(name + " taking connection from other pool");
        }

    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class DataWriter implements Runnable {

    String name;
    DatabaseConnectionPool pool;

    @Override
    public void run() {
        // pool.getConnection(name);
        pool.writeData(name);

    }

}