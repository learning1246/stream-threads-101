package com.practice.practice.oct.a20221018;

import com.practice.practice.oct.a20221018.BuilderDemo.BuilderDemoBuilder;

public class Creational {

    public static void main(String[] args) {
        new BuilderDemoBuilder().setDescription("haha").setName("name").build();
    }

}

class SingletonDemo {
    private static SingletonDemo instance = new SingletonDemo();

    private SingletonDemo() {

    }

    public static SingletonDemo getInstance() {
        return instance;
    }
}

class BuilderDemo {

    String name;

    String description;

    private BuilderDemo(BuilderDemoBuilder builder) {
        this.name = builder.name;
        this.description = builder.description;
    }

    public static class BuilderDemoBuilder {
        String name;

        String description;

        public BuilderDemoBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public BuilderDemoBuilder setDescription(String name) {
            this.description = name;
            return this;
        }

        public BuilderDemo build() {
            return new BuilderDemo(this);
        }
    }
}