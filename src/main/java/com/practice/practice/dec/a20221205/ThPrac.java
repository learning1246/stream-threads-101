package com.practice.practice.dec.a20221205;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Person;

public class ThPrac {
    public static void main(String[] args) {
        Object collect = MockData.getPersons()
                .stream()
                .map(Person::getSalary)
                .reduce(0.0, Math::max);
        System.out.println(collect);
    }
}
