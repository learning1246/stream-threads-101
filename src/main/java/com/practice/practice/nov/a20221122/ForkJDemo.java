package com.practice.practice.nov.a20221122;

import java.util.concurrent.RecursiveTask;

import com.practice.practice.oct.a20221011.ForkJoinPool;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class ForkJDemo {

    public static void main(String[] args) {
        Integer invoke = new Rec(5).invoke();
        System.out.println(invoke);
    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Rec extends RecursiveTask<Integer> {

    int data;

    @Override
    protected Integer compute() {
        if (data <= 1) {
            return 1;
        }
        Rec f1 = new Rec(data - 1);
        f1.fork();
        Rec f2 = new Rec(data - 2);
        f2.fork();

        return f1.join() + f2.join();
    }

}
