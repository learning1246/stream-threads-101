package com.practice.practice.nov.a20221122;

import java.util.Comparator;
import java.util.TreeMap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class TreeDemo {
    public static void main(String[] args) {

        TreeMap<Customer, String> map = new TreeMap<>(Comparator.comparing(Customer::getId));
        map.put(new Customer(1, "Dhaanjay"), "Dhaanjay");
        map.put(new Customer(20, "Dipali"), "Diapli");
        map.put(new Customer(12, "Ridhvi"), "Ridhvi");
        System.out.println(map);
    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Customer {
    int id;
    String name;
}
