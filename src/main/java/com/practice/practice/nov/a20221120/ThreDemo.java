package com.practice.practice.nov.a20221120;

import java.util.Comparator;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class ThreDemo {
    public static void main(String[] args) {

        Object collect = MockData.fetchEmployees()
                .stream()
                .filter(e -> e.getNewJoiner().equalsIgnoreCase("FALSE"))
                .limit(5)
                .sorted(Comparator.comparing(Employee::getFirstName))
                .map(Employee::getFirstName)
                .collect(Collectors.toList());

        System.out.println(collect);

        // Ghat ghat = new Ghat();
        // Ghat ghat2 = new Ghat();
        // new Thread(new ThreadX(ghat, "ridhvi")).start();
        // new Thread(new ThreadX(ghat, "shreya")).start();
        // new Thread(new ThreadX(ghat, "arohi")).start();

    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class ThreadX implements Runnable {

    Ghat ghat;

    String name;

    @Override
    public void run() {
        // TODO Auto-generated method stub

        ghat.ascend(name);

    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Police {

    Ghat ghat;

    public void openTraffic() {
        synchronized (ghat) {
            ghat.notify();
        }
    }

}

class Ghat {

    Semaphore lock = new Semaphore(2);

    public void ascend(String name) {
        System.out.println(name + " is now ascending");
        MockData.delay(5);
        try {
            lock.acquire();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(name + " is now started ");
        MockData.delay(5);
        lock.release();
        System.out.println(name + " is now finished");
    }

}