package com.practice.practice.nov.a20221126;

import java.util.ArrayList;
import java.util.Arrays;

public class BinarySerch {
    public static void main(String[] args) {
        // int input[] = { 4, 2, 7, 5, 0, 3, 5, 10 };
        int input[] = { 0, 2, 10, 3, 10, 10, 15, 1 };
        mergeSort(input, 0, input.length - 1);
        for (int k : input) {
            System.out.print(k + " ");
        }
        System.out.println();
        System.out.println(binSearch(input, 0, input.length - 1, 10));

    }

    public static void mergeSort(int arr[], int l, int r) {

        if (l < r) {
            int mid = (l + r) / 2;
            mergeSort(arr, l, mid);
            mergeSort(arr, mid + 1, r);
            merge(arr, l, mid, r);
        }

    }

    private static void merge(int[] arr, int l, int mid, int r) {
        int i = l;
        int j = mid + 1;
        int k = l;
        int b[] = new int[arr.length];
        while (i <= mid && j <= r) {
            if (arr[i] < arr[j]) {
                b[k] = arr[i];
                i++;
            } else {
                b[k] = arr[j];
                j++;
            }
            k++;
        }
        if (i > mid) {
            while (j <= r) {
                b[k] = arr[j];
                j++;
                k++;
            }
        } else {
            while (i <= mid) {
                b[k] = arr[i];
                i++;
                k++;
            }
        }

        for (int x = l; x <= r; x++) {
            arr[x] = b[x];
        }
    }

    private static void swap(int[] arr, int j, int l) {
        int temp = arr[j];
        arr[j] = arr[l];
        arr[l] = temp;
    }

    private static ArrayList<Integer> binSearch(int[] arr, int l, int r, int target) {
        int i = l;
        int j = r;
        Arrays.sort(arr);
        ArrayList<Integer> result = new ArrayList<>();
        while (i <= j) {
            int mid = (i + j) / 2;
            if (arr[mid] == target) {
                int x = mid - 1;
                while (x >= 0 && arr[x] == target) {
                    result.add(x);
                    x--;
                }
                result.add(mid);
                x = mid + 1;
                while (x <= r && arr[x] == target) {
                    result.add(x);
                    x++;
                }
                return result;
            } else if (arr[mid] < target) {
                i = mid + 1;
            } else {
                j = mid - 1;
            }
        }
        return new ArrayList<>();
    }

    public static void quiS(int arr[], int l, int r) {
        if (l < r) {
            int pivot = partition(arr, l, r);
            quiS(arr, l, pivot - 1);
            quiS(arr, pivot + 1, r);
        }
    }

    public static int partition(int[] arr, int l, int r) {
        int i = l;
        int j = r;
        int pivot = arr[l];
        while (i < j) {
            while (i < l && arr[i] <= pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }
            if (i < j) {
                swap(arr, i, j);
            }
        }
        swap(arr, l, j);
        return j;
    }
}
