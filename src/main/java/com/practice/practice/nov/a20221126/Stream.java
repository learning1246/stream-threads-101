package com.practice.practice.nov.a20221126;

import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;

public class Stream {
    public static void main(String[] args) {
        CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> {
            return MockData.fetchEmployees();
        }).thenApply(l -> {
            return l.stream().filter(e -> e.getGender().equalsIgnoreCase("female"))
                    .collect(Collectors.toList());
        }).thenApply(l -> {
            return l.stream().filter(e -> e.getNewJoiner().equalsIgnoreCase("true"))
                    .collect(Collectors.toList());
        }).thenApply(l -> {
            return l.stream().limit(5).map(Employee::getEmail).collect(Collectors.toList());
        }).thenAccept(l -> l.stream().forEach(System.out::println));

        try {
            future.get();
        } catch (InterruptedException | ExecutionException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }
}
