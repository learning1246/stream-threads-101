package com.practice.practice.nov.a20221112;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

import com.practice.practice.data.MockData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class Gh {

    public static void main(String[] args) {
        CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> {
            return MockData.fetchEmployees();
        }).thenApply((l) -> {
            System.out.println("hh");
            return l.stream().filter(f -> f.getNewJoiner().equalsIgnoreCase("FALSE")).collect(Collectors.toList());
        }).thenAccept((ls) -> {
            System.out.println("GG");
            System.out.println(ls.size());
        });
        try {
            future.get();
        } catch (InterruptedException | ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void main2(String[] args) {

        ExecutorService es = Executors.newCachedThreadPool();

        CountDownLatch cl = new CountDownLatch(3);
        Runnable rArray[] = {
                new Rocket(cl),
                new DepartmentThread("Fuel", 2, cl),
                new DepartmentThread("Launchpanel", 5, cl),
                new DepartmentThread("BRB", 10, cl)
        };

        for (Runnable r : rArray) {
            es.execute(r);
        }

        es.shutdown();

    }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class DepartmentThread implements Runnable {

    String department;

    int delay;

    CountDownLatch cl;

    @Override
    public void run() {
        MockData.delay(delay);
        System.out.println("Department Ready : " + department);
        cl.countDown();
    }

}

@Data
@AllArgsConstructor
@NoArgsConstructor
class Rocket implements Runnable {

    CountDownLatch cl;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        try {
            cl.await();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        MockData.delay(1);
        System.out.println("Launching....\n3");
        MockData.delay(1);
        System.out.println("2");
        MockData.delay(1);
        System.out.println("1");
        MockData.delay(1);
        System.out.println("GO falcon heavy");
    }

}

class DataContext {
    public static ThreadLocal<String> data = new ThreadLocal<>();
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Elephant implements Runnable {

    ElephantService es = new ElephantService();

    String name;

    @Override
    public void run() {
        DataContext.data.set(name);

        es.sericeMethod();
    }
}

class ElephantService {

    public void sericeMethod() {
        System.out.println("Data in sevice" + DataContext.data.get());
    }

}

class ChRecursiveTask extends RecursiveTask<Integer> {

    int number;

    ChRecursiveTask(int number) {
        this.number = number;
    }

    @Override
    protected Integer compute() {

        if (number <= 1) {
            return number;
        }
        ChRecursiveTask f1 = new ChRecursiveTask(number - 1);
        f1.fork();
        ChRecursiveTask f2 = new ChRecursiveTask(number - 2);
        f2.fork();

        return f1.join() + f2.join();
    }

}
