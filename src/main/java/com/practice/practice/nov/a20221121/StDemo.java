package com.practice.practice.nov.a20221121;

import java.util.List;
import java.util.stream.Collectors;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Person;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class StDemo {

    static ThreadLocal<String> context = new ThreadLocal<String>() {

        @Override
        protected String initialValue() {
            return "NotSet";
        }

    };

    public static void main(String[] args) {
        new Thread(new H("Rdvi")).start();
    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class H implements Runnable {

    String name;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        MockData.delay(2);
        new HService().callEmail();
        StDemo.context.set(name);
        new HService().callEmail();
        StDemo.context.remove();
        new HService().callEmail();
    }

}

class HService {
    void callEmail() {
        System.out.println("Inside HService called by : " + StDemo.context.get());
    }
}
