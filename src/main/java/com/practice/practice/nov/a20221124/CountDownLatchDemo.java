package com.practice.practice.nov.a20221124;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.stream.Stream;

import com.practice.practice.data.MockData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class CountDownLatchDemo {

    public static void main(String[] args) {

        CyclicBarrier cl = new CyclicBarrier(4);

        CDL[] array = {
                new CDL("Item Service", new Random().nextInt(10), cl),
                new CDL("Customer Service", new Random().nextInt(10), cl),
                new CDL("Loyalty Service", new Random().nextInt(10), cl)
        };
        for (CDL c : array) {
            new Thread(c).start();
        }
        try {
            cl.await();
            System.out.println("Rest service can continue now");
        } catch (InterruptedException | BrokenBarrierException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class CDL implements Runnable {
    String serviceName;

    int delay;

    CyclicBarrier cl;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        System.out.println("Calling Service: " + serviceName);
        MockData.delay(delay);
        System.out.println("Recived Service: " + serviceName);
        try {
            cl.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Await finished Service: " + serviceName);
    }
}