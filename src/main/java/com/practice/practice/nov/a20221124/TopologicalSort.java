package com.practice.practice.nov.a20221124;

import java.util.ArrayList;
import java.util.Stack;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class TopologicalSort {

    public static void main(String[] args) {
        int vtces = 5;
        ArrayList<EdgeD>[] graph = buildGraph(vtces);
        boolean visited[] = new boolean[vtces];
        Stack<Integer> stack = new Stack<Integer>();
        for (int i = 0; i < vtces; i++) {
            if (visited[i] == false) {
                topoSort(graph, visited, i, stack);
            }
        }

        while (!stack.isEmpty()) {
            System.out.println(stack.pop());
        }

    }

    private static void topoSort(ArrayList<EdgeD>[] graph, boolean[] visited, int i, Stack<Integer> stack) {
        visited[i] = true;
        for (EdgeD e : graph[i]) {
            if (visited[e.nbr] == false) {
                topoSort(graph, visited, e.nbr, stack);
            }
        }
        stack.push(i);
    }

    public static ArrayList<EdgeD>[] buildGraph(int vtces) {
        ArrayList<EdgeD>[] graph = new ArrayList[vtces];

        for (int i = 0; i < vtces; i++) {
            graph[i] = new ArrayList<>();
        }

        graph[2].add(new EdgeD(2, 3));

        graph[3].add(new EdgeD(3, 0));

        graph[0].add(new EdgeD(0, 1));

        graph[1].add(new EdgeD(1, 4));
        return graph;
    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class EdgeD {
    int v;
    int nbr;
}
