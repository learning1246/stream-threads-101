package com.practice.practice.nov.a20221124;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class Dijkstra {

    public static void main(String[] args) {
        int vtces = 7;
        ArrayList<Edge>[] graph = buildGraph(vtces);

        // HashSet<Integer> visited = new HashSet<>();
        // boolean hasPath = hasPath(graph, visited, 0, 10, "");
        // System.out.println(hasPath);
        boolean[] visited = new boolean[vtces];
        // printAllPaths(graph, visited, 0, 6, "0");

        // printAllPathsWithWeights(graph, visited, 0, 6, "0", 0);
        // dijstra(graph, visited, 0, 6);

    }

    private static void dijstra(ArrayList<Edge>[] graph, boolean[] visited, int src, int dest) {
        PriorityQueue<Pair> queue = new PriorityQueue<>(Comparator.comparing(Pair::getW));
        queue.add(new Pair(0, "0", 0));
        while (!queue.isEmpty()) {
            Pair removed = queue.poll();
            if (visited[removed.v]) {
                continue;
            }
            visited[removed.v] = true;
            System.out.println(removed.psf + "@" + removed.w);
            for (Edge e : graph[removed.v]) {
                if (!visited[e.nbr]) {
                    queue.add(new Pair(e.nbr, removed.psf + e.nbr, removed.w + e.weight));
                }
            }
        }
    }

    private static void bfs(ArrayList<Edge>[] graph, boolean[] visited, int src) {
        Queue<Pair> queue = new LinkedList<>();
        queue.add(new Pair(0, "0", 0));
        while (!queue.isEmpty()) {
            Pair removed = queue.poll();
            if (visited[removed.v]) {
                continue;
            }
            visited[removed.v] = true;
            System.out.println(removed.v + "@" + removed.psf);
            for (Edge e : graph[removed.v]) {
                if (!visited[e.nbr]) {
                    queue.add(new Pair(e.nbr, removed.psf + e.nbr, 0));
                }
            }
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class Pair {
        int v;
        String psf;
        int w;
    }

    private static void printAllPaths(ArrayList<Edge>[] graph, boolean[] visited, int src, int dest,
            String psf) {
        if (src == dest) {
            System.out.println(psf);
            return;
        }
        visited[src] = true;
        for (Edge edge : graph[src]) {
            if (!visited[edge.nbr]) {
                printAllPaths(graph, visited, edge.nbr, dest, psf + edge.nbr);
            }
        }
        visited[src] = false;
    }

    private static void printAllPathsWithWeights(ArrayList<Edge>[] graph, boolean[] visited, int src, int dest,
            String psf, int weight) {
        if (src == dest) {
            System.out.println(psf + "@" + weight);
            return;
        }
        visited[src] = true;
        for (Edge edge : graph[src]) {
            if (!visited[edge.nbr]) {
                printAllPathsWithWeights(graph, visited, edge.nbr, dest, psf + edge.nbr, weight + edge.weight);
            }
        }
        visited[src] = false;
    }

    private static boolean hasPath(ArrayList<Edge>[] graph, HashSet<Integer> visited, int src, int dest,
            String string) {
        if (src == dest) {
            return true;
        }
        visited.add(src);
        for (Edge edge : graph[src]) {
            if (!visited.contains(edge.nbr)) {
                boolean hasPath = hasPath(graph, visited, edge.nbr, dest, string);
                if (hasPath) {
                    return true;
                }
            }
        }

        return false;
    }

    // 0---|2|--->1---|4|--->4---|7|--->5
    // . . . . |
    // . 3 6 6 3
    // 2 . . . |
    // . . . . >
    // 2---|5|---> 3 6
    public static ArrayList<Edge>[] buildGraph(int vtces) {
        ArrayList<Edge>[] graph = new ArrayList[vtces];

        for (int i = 0; i < vtces; i++) {
            graph[i] = new ArrayList<>();
        }

        graph[0].add(new Edge(0, 2, 2));
        graph[0].add(new Edge(0, 1, 2));

        graph[1].add(new Edge(1, 4, 4));

        graph[2].add(new Edge(2, 1, 3));
        graph[2].add(new Edge(2, 3, 5));

        graph[3].add(new Edge(3, 4, 6));
        graph[3].add(new Edge(3, 5, 8));

        graph[4].add(new Edge(4, 5, 7));

        graph[5].add(new Edge(5, 6, 3));
        return graph;
    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Edge {
    int src;
    int nbr;
    int weight;
}