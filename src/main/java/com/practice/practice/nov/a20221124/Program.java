package com.practice.practice.nov.a20221124;

import java.util.*;

class Program {
    public int[] dijkstrasAlgorithm(int start, int[][][] edges) {
        // Write your code here.
        PriorityQueue<Pair> queue = new PriorityQueue<>(Comparator.comparing(Pair::getW));
        int[] result = new int[edges.length];
        boolean visited[] = new boolean[edges.length];
        queue.add(new Pair(start, 0));

        while (queue.size() > 0) {
            Pair removed = queue.poll();
            int weight = removed.getW();
            int v = removed.getV();
            if (visited[v] == true) {
                continue;
            }
            visited[v] = true;
            for (int[] arr : edges[v]) {
                int nbr = arr[0];
                int weg = arr[1];
                if (visited[nbr] == false) {
                    queue.add(new Pair(nbr, weg + weight));
                }
            }

        }
        return new int[] {};
    }

    static class Pair {
        int v;
        int w;

        Pair(int v, int w) {
            this.v = v;
            this.w = w;
        }

        int getV() {
            return v;
        }

        int getW() {
            return this.w;
        }
    }
}
