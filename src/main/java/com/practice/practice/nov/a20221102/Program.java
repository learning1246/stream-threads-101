package com.practice.practice.nov.a20221102;

import java.util.*;

class Program {

    public static void main(String[] args) {
        
       System.out.println(isMonotonic(new int[]{1,2}));
    }

    public static boolean isMonotonic(int[] array) {
        // Write your code here.
        if(array.length<=2){
          return true;
        }
        boolean incrementing = true;
        boolean decrementing = true;
        for(int i=1;i<array.length;i++){
          if(array[i-1]<array[i]){
            incrementing =false;
          }
          if(array[i-1]>array[i]){
            decrementing =false;
          }
        }
        
        return incrementing || decrementing;
      }

    public static List<Integer> moveElementToEnd(List<Integer> array, int toMove) {
        // Write your code here.
        int x = 0;
        int y = array.size() - 1;
        while (x < y) {
            while (array.get(y) == toMove && x < y) {
                y--;
            }
            System.out.println("x: "+array.get(x));
            if (array.get(x) == toMove) {
                int temp = array.get(x);
                array.set(x, array.get(y));
                array.set(y, temp);
                x++;
                y--;
            } else {
                x++;
            }
        }
        return array;
    }
}
