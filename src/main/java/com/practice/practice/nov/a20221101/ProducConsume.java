package com.practice.practice.nov.a20221101;

import java.util.Comparator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class ProducConsume {

    public static void main(String[] args) {

        // ArrayBlockingQueue queue = new ArrayBlockingQueue<>(10);

        // ScheduledExecutorService es = Executors.newScheduledThreadPool(2);
        // es.schedule(new Produ(queue), 10, TimeUnit.SECONDS);
        // es.schedule(new Consume(queue), 5, TimeUnit.SECONDS);

        // es.shutdown();

        Object o = MockData.fetchEmployees()
                .stream()
                .filter(e -> e.getGender().equalsIgnoreCase("female"))
                .limit(10)
                .sorted(Comparator.comparing(Employee::getFirstName).reversed())
                .mapToInt(Employee::getRating).max();
        System.out.println(o);

    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Produ implements Runnable {

    ArrayBlockingQueue queue;

    @Override
    public void run() {
        while (true) {
            Double random = Math.random();
            System.out.println("Producer adding random object" + random);
            if (queue.size() < 10) {
                queue.add(random);
                synchronized (queue) {
                    queue.notify();
                }
            }
            MockData.delay(5);

        }
    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Consume implements Runnable {

    ArrayBlockingQueue queue;

    @Override
    public void run() {
        while (true) {
            synchronized (queue) {
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            System.out.println("Consumer got from queue:" + queue.poll());
        }
    }

}
