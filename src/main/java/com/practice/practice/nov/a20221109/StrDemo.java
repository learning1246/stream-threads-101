package com.practice.practice.nov.a20221109;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Employee;
import com.practice.practice.dto.Person;

public class StrDemo {

    public static void main(String[] args) {

        Object objec = MockData.getPersons().stream()
                .collect(Collectors.groupingBy(Person::getRole,
                        Collectors.mapping(Person::getName, Collectors.toList())));
        System.out.println(objec);
    }

}
