package com.practice.practice.nov.a20221117;

import java.util.Arrays;

import org.springframework.util.SystemPropertyUtils;

public class SortDemo {

    public static void main(String[] args) {
        int input[] = { 'A', 'X', 'J', '5', '7', '1', 'a', 'v', 'b' };

        Arrays.sort(input);
        for (int i : input) {
            System.out.println((char) i);
        }
    }

}
