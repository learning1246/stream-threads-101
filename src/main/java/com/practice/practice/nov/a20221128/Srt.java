package com.practice.practice.nov.a20221128;

import com.practice.practice.nov.a20221125.QuickSort;

public class Srt {

    public static void main(String[] args) {
        int input[] = { 0, 2, 10, 3, 10, 10, 15, 1, 1 };
        mergeS(input, 0, input.length - 1);
        for (int k : input) {
            System.out.print(k + " ");
        }
    }

    public static void mergeS(int arr[], int l, int r) {
        if (l < r) {
            int mid = (l + r) / 2;
            mergeS(arr, l, mid);
            mergeS(arr, mid + 1, r);
            merge(arr, l, mid, r);
        }
    }

    private static void merge(int[] arr, int l, int mid, int r) {
        int i = l;
        int j = mid + 1;
        int k = l;
        int b[] = new int[arr.length];
        while (i <= mid && j <= r) {
            if (arr[i] < arr[j]) {
                b[k] = arr[i];
                i++;
            } else {
                b[k] = arr[j];
                j++;
            }
            k++;
        }
        if (i > mid) {
            while (j <= r) {
                b[k] = arr[j];
                j++;
                k++;
            }
        } else {
            while (i <= mid) {
                b[k] = arr[i];
                i++;
                k++;
            }
        }
        for (int x = l; x <= r; x++) {
            arr[x] = b[x];
        }
    }

    private static void swap(int[] arr, int j, int l) {
        int temp = arr[j];
        arr[j] = arr[l];
        arr[l] = temp;
    }

    public static void quickS(int arr[], int l, int r) {
        if (l < r) {
            int pivot = partition(arr, l, r);
            quickS(arr, l, pivot - 1);
            quickS(arr, pivot + 1, r);
        }
    }

    public static int partition(int arr[], int l, int r) {
        int i = l;
        int j = r;
        int pivot = arr[l];
        while (i < j) {
            while (i < r && arr[i] <= pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }
            if (i < j) {
                swap(arr, i, j);
            }
        }
        swap(arr, l, j);
        return j;
    }
}
