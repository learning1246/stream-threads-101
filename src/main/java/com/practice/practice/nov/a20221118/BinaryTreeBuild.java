package com.practice.practice.nov.a20221118;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

import javax.xml.crypto.dsig.keyinfo.RetrievalMethod;

import org.springframework.util.SystemPropertyUtils;

public class BinaryTreeBuild {
    static Scanner sc;

    BinaryTreeBuild() {

    }

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        BTreeNode root = createTree();

        Queue<BTreeNode> queue = new LinkedList<>();
        queue.add(root);
        levelOrderTraversal(root,queue);

    }

    private static void levelOrderTraversal(BTreeNode root, Queue<BTreeNode> queue) {
        while(!queue.isEmpty()){
            BTreeNode n = queue.poll();
            System.out.println(n.val);
            if(n.left!=null){
                queue.add(n.left);
            }
            if(n.right!=null){
                queue.add(n.right);
            }            
        }
    }

    private static void inOrderTraversal(BTreeNode root) {
        if (root == null) {
            return;
        }
        inOrderTraversal(root.left);
        System.out.println(root.val);
        inOrderTraversal(root.right);

    }

    private static BTreeNode createTree() {
        BTreeNode root = null;

        System.out.println("Enter Data:");
        int nextInt = sc.nextInt();
        if (nextInt == -1) {
            return null;
        }

        root = new BTreeNode(nextInt);

        System.out.println("Enter left for :" + nextInt);
        root.left = createTree();

        System.out.println("Enter right for :" + nextInt);
        root.right = createTree();

        return root;
    }
}

class BTreeNode {

    int val;
    BTreeNode left, right;

    BTreeNode(int val) {
        this.val = val;
    }

}