package com.practice.practice.nov.a20221130;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import com.practice.practice.data.MockData;

public class Stre {

    public static void main(String[] args) {
        // Object collect = MockData.getPersons()
        // .stream()
        // .collect(Collectors.groupingBy(Person::getRole,
        // Collectors.summingDouble(Person::getSalary)));
        // System.out.println(collect);

        ScheduledExecutorService es = Executors.newScheduledThreadPool(5);
        IntStream.range(0, 5).forEach(x -> {
            Future f = es.schedule(new AT(), 2, TimeUnit.SECONDS);
            try {
                System.out.println(
                        f.get());
            } catch (InterruptedException | ExecutionException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });

        es.shutdown();
    }

}

class AT implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println(Thread.currentThread().getName() + " Started");
        int j = new Random().nextInt(10);
        MockData.delay(new Random().nextInt(10));
        System.out.println(Thread.currentThread().getName() + " Ended");
        return j;
    }

}