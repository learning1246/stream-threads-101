package com.practice.practice.nov.a20221125;

public class RevSort {

    public static void main(String[] args) {
        int input[] = { 4, 2, 7, 5, 0, 3, 5, 10 };

        mergeSort(input, 0, input.length - 1);
        for (int k : input) {
            System.out.println(k);
        }
    }

    public static void mergeSort(int[] arr, int l, int r) {
        if (l < r) {
            int mid = (l + r) / 2;
            mergeSort(arr, l, mid);
            mergeSort(arr, mid + 1, r);
            merge(arr, l, mid, r);
        }
    }

    private static void merge(int[] arr, int l, int mid, int r) {
        int i = l;
        int j = mid + 1;
        int k = l;
        int b[] = new int[arr.length];
        while (i <= mid && j <= r) {
            if (arr[i] < arr[j]) {
                b[k] = arr[i];
                i++;
            } else {
                b[k] = arr[j];
                j++;
            }
            k++;
        }
        if (i > mid) {
            while (j <= r) {
                b[k] = arr[j];
                k++;
                j++;
            }
        } else {
            while (i <= mid) {
                b[k] = arr[i];
                k++;
                i++;
            }
        }
        for (int m = l; m <= r; m++) {
            arr[m] = b[m];
        }
    }

    private static void swap(int[] arr, int j, int l) {
        int temp = arr[j];
        arr[j] = arr[l];
        arr[l] = temp;
    }

    public static void quickSort(int arr[], int l, int h) {
        if (l < h) {
            int pivot = partition(arr, l, h);
            quickSort(arr, l, pivot - 1);
            quickSort(arr, pivot + 1, h);
        }
    }

    public static int partition(int arr[], int l, int h) {
        int i = l;
        int j = h;
        int pivot = arr[l];
        while (i < j) {
            while (arr[i] <= pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }
            if (i < j) {
                swap(arr, j, i);
            }
        }
        swap(arr, j, l);
        return j;
    }

}
