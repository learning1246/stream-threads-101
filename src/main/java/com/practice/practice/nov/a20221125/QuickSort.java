package com.practice.practice.nov.a20221125;

public class QuickSort {

    public static void main(String[] args) {
        int input[] = { 4, 2, 7, 5, 0, 3, 5, 10 };
        int i = 0;
        int j = input.length - 1;
        // quickSort(input, i, j);
        mergeSort(input, i, j);
        for (int k : input) {
            System.out.println(k);
        }
    }

    public static void mergeSort(int[] input, int l, int r) {
        if (l < r) {
            int mid = (l + r) / 2;
            mergeSort(input, l, mid);
            mergeSort(input, mid + 1, r);
            merge(input, l, mid, r);
        }
    }

    private static void merge(int[] arr, int l, int mid, int r) {
        int i = l;
        int j = mid + 1;
        int k = l;
        int b[] = new int[arr.length];
        while (i <= mid && j <= r) {
            if (arr[i] < arr[j]) {
                b[k] = arr[i];
                i++;
            } else {
                b[k] = arr[j];
                j++;
            }
            k++;
        }
        if (i > mid) {
            while (j <= r) {
                b[j] = arr[j];
                j++;
                k++;
            }
        } else {
            while (i <= mid) {
                b[k] = arr[i];
                i++;
                k++;
            }
        }
        for (int g = l; g <= r; g++) {
            arr[g] = b[g];
        }
    }

    private static void quickSort(int[] input, int i, int j) {
        if (i < j) {
            int pivot = partition(input, i, j);
            quickSort(input, i, pivot - 1);
            quickSort(input, pivot + 1, j);
        }
    }

    public static int partition(int arr[], int l, int h) {
        int pivot = arr[l];
        int i = l;
        int j = h;
        while (i < j) {
            while (arr[i] <= pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }
            if (i < j) {
                swap(arr, i, j);
            }
        }
        swap(arr, j, l);
        return j;
    }

    private static void swap(int[] arr, int j, int l) {
        int temp = arr[j];
        arr[j] = arr[l];
        arr[l] = temp;
    }
}
