package com.practice.practice.nov.a20221115;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import com.practice.practice.data.MockData;
import com.practice.practice.dto.Person;

public class ES {

    public static void main(String[] args) {
        Object collect = MockData.getPersons().parallelStream().map(Person::getAge).reduce(0,(a,b)-> a+ b);
        System.out.println(collect);

    }

}

class ESThread implements Callable {

    @Override
    public Object call() {
        System.out.println("Started : " + Thread.currentThread().getName());
        MockData.delay(2);
        System.out.println("Finished : " + Thread.currentThread().getName());
        return new Random().nextInt();
    }

}