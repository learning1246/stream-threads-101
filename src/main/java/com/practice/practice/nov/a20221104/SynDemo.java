package com.practice.practice.nov.a20221104;

import com.practice.practice.data.MockData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class SynDemo {

    public static void main(String[] args) {
        Object lock = new Object();
        new Thread(new Waiter(lock, "DJ")).start();
        new Thread(new Waiter(lock, "Ridhi")).start();
        new Thread(new Postman(lock)).start();
    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Postman implements Runnable {

    Object lock;

    @Override
    public void run() {
        MockData.delay(10);
        synchronized (lock) {
            System.out.println("POstman calling notifuy");
            lock.notifyAll();
        }
    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Waiter implements Runnable {

    Object lock;
    String name;

    @Override
    public void run() {
        synchronized (lock) {
            try {
                System.out.println("waiter " + name + " waiting");
                lock.wait(20000);
                System.out.println("waiter " + name + " invoked");
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
