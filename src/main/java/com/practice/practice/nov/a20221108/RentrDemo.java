package com.practice.practice.nov.a20221108;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Stream;

import com.practice.practice.data.MockData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class RentrDemo {

    public static void main(String[] args) {
        Ghat ghat = new Ghat();
        Stream.of(new RE(ghat, "DJ"), new RE(ghat, "Ridhvi"), new RE(ghat, "Dips")).forEach(c -> new Thread(c).start());
        ;
    }

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class RE implements Runnable {

    Ghat ghat;
    String name;

    @Override
    public void run() {
        // TODO Auto-generated method stub

        ghat.ascend(name);

    }

}

class Ghat {

    Semaphore lock = new Semaphore(2);

    public void ascend(String name) {

        System.out.println("waiting to ascend " + name );
        try {
            lock.acquire();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        descned(name);
    }

    public void descned(String name) {
        System.out.println("started to ascend " + name );
        MockData.delay(5);
        lock.release();
        System.out.println(name + " descendinged");
    }
}