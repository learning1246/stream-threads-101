package com.practice.practice.nov.a20221108;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.practice.practice.dto.Employee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class FindManager {

    public static void main(String[] args) {
        int[][] input = {{0,0},{2,1},{4,3},{3,2},{1,0},{4,4}};
        boolean[] visited = new boolean[5];

        for(int[] entry:input){

        }


        Map<Integer,Integer> managerMap = new HashMap<>();
        Map<Integer,Integer> employeeMap = new HashMap<>();

        // LinkedList<Relation> ll= new LinkedList<>();
        // for(int[] entry:input){
        //     managerMap.put(entry[1],entry[0]);
        //     employeeMap.put(entry[0],entry[1]);
        // }

        // findManager(input[0][0],managerMap,employeeMap);
        // findEmployee(managerMap.get(input[0][1]),managerMap,employeeMap);
    }

    static void findManager(int employeeId, Map<Integer,Integer> managerMap,Map<Integer,Integer> employeeMap){
        Integer managerId = employeeMap.get(employeeId);
        if(managerId == employeeId){
            System.out.print(managerId);
            System.out.print("->");
            return;
        }else{
            findManager(managerId, managerMap, employeeMap);
            System.out.print(employeeId);
            System.out.print("->");
        }
                
    }

    static void findEmployee(int managerId, Map<Integer,Integer> managerMap,Map<Integer,Integer> employeeMap){
        Integer employeeId = managerMap.get(managerId);
        if(employeeId==managerId){
            return;
        }
        System.out.print(employeeId +"->");
        findEmployee(employeeId,managerMap,employeeMap);
    }
}


@Data
@NoArgsConstructor
@AllArgsConstructor
class Relation {
    int employeeId;
    int managerId;
}