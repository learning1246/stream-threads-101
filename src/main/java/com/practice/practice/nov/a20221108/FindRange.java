package com.practice.practice.nov.a20221108;

import java.util.ArrayList;
import java.util.List;

public class FindRange {
    
    public static void main(String[] args) {
        //int input[] = {1,2,3,4,6,8,9,10,11,12,19,20,21};
        int input[] = {1,2,3,4,5,6,7,8,9,10};
        //int input[] = {12,19,20};
        //output = {1-4,6,8-12,16,20}
        List<String> result= new ArrayList<>();
        int startIndex =0;
        int endIndex =0;
        int lastNo = input[0];
        for(int i=1;i<input.length;i++){
           if(input[i]-input[i-1]==1){
             endIndex++;
           }else{
             if(startIndex==endIndex){
                result.add(""+input[endIndex]);
                startIndex=i;
                endIndex=i;
             }else{
                result.add(input[startIndex]+"-"+input[endIndex]);
                startIndex=i;
                endIndex=i;
             }
           }
        }
        if(startIndex==endIndex){
            result.add(""+input[endIndex]);
            
         }else{
            result.add(input[startIndex]+"-"+input[endIndex]);
           
         }
        System.out.println(result);
    }
}
