package com.practice.practice.nov.a20221105;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class StDemo {

    public static void main(String[] args) {
        Stack stack = new Stack<>();
        
    }

    public ArrayList<Integer> sortStack(ArrayList<Integer> stack) {
        // Write your code here.
        if(stack.size()==1){
          ArrayList<Integer>  list = new ArrayList<>();
          list.add(stack.get(stack.size()-1));
          return list;
        }
        int poped = stack.remove(stack.size()-1);
        ArrayList<Integer> sorted = sortStack(stack);
        if(sorted.get(sorted.size()-1)>poped){
            int greater = sorted.remove(sorted.size()-1);
            sorted.add(poped);
            sorted.add(greater);
        }else{
            sorted.add(poped);
        }
        return sorted;
    }
      
    public ArrayList<Integer> sunsetViews(int[] buildings, String direction) {
        // Write your code here.
        ArrayList<Integer> result = new ArrayList<>();
        Stack<Integer> stack = new Stack<>();
        if (direction.equals("EAST")) {
            for (int i = buildings.length - 1; i > 0; i--) {
                if (stack.size() == 0) {
                    stack.push(i);
                    continue;
                }
                if (buildings[i] > buildings[stack.peek()]) {
                    stack.push(i);
                }
            }
        } else {
            for (int i = 0; i< buildings.length; i++) {
                if (stack.size() == 0) {
                    stack.push(i);
                    continue;
                }
                if (buildings[i] > buildings[stack.peek()]) {
                    stack.push(i);
                }
            }
        }
        while(!stack.isEmpty()){
            result.add(stack.pop());
        }
        return result;
    }

    public static boolean balancedBrackets(String str) {
        // Write your code here.
        Stack<Character> stack = new Stack<>();
        for (char c : str.toCharArray()) {
            switch (c) {
                case '[':
                    stack.push('[');
                    break;
                case '{':
                    stack.push('{');
                    break;
                case '(':
                    stack.push('(');
                    break;
                case '}': {
                    if (stack.pop() == '{') {
                        continue;
                    } else {
                        return false;
                    }
                }
                case ']': {
                    if (stack.pop() == '[') {
                        continue;
                    } else {
                        return false;
                    }
                }
                case ')': {
                    if (stack.pop() == '(') {
                        continue;
                    } else {
                        return false;
                    }
                }
            }
        }
        return stack.isEmpty();
    }

}
