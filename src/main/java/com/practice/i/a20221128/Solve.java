package com.practice.i.a20221128;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Solve {

    public static void main(String[] args) {
        // using Stream APIs need to extract the names of employees which starts with
        // characters "Su" or any string.
        // Assume the query string coming from UI fields.
        // (it should be case insensitive )

        // Suresh
        // sumathi

        System.out.println(filterData("su", Arrays.asList("Suresh", "sumathi", "suyog")));
    }

    public static List<String> filterData(String prefix, List<String> employeeList) {

        // Predicate<String> prefixFilter = (s) ->
        return employeeList.stream().filter(name -> name.toLowerCase().startsWith(prefix.toLowerCase()))
                .collect(Collectors.toList());

        // find the max salaried employee name from list of employees (Salary is a
        // column in Employee table)

        // select name from employee order by salary desc limit 1

    }

}

// "select c from Employee where id = 0?"

class Employee {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

class Program {
    static int a = 4;
    static int b = 3;

    public static void main(String[] args) {
        a = 10;
        b = 20;
    }

}

// II
class Program2 {
    static int a = 4;
    int b = 3;

    public static void main(String[] args) {
        Program2 p = new Program2();

        a = 10;
        p.b = 20;
    }

}